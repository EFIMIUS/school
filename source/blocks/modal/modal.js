class Start {
  constructor() {
    const modal = document.querySelector('.modal');
    const btn = document.querySelector('.modal__button');
    const checks = document.querySelectorAll('.question__input');

    btn.addEventListener('click', () => {
      window.scrollTo(0, 0);
      let active;
      checks.forEach((check) => {
        active = check;
        active.checked = false;
      });
      modal.classList.add('modal--hidden');
      setTimeout(() => {
        document.body.classList.remove('scroll-hidden');
        modal.classList.add('modal--disable');
      }, 550);
    });
  }
}

export default Start;
