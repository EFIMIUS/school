import Result from '../question/question';

class End {
  constructor() {
    const btn = document.querySelector('.end__button');
    const modal = document.querySelector('.modal');
    const end = document.querySelector('.end');
    this.result = new Result();

    btn.addEventListener('click', () => {
      this.result.resA = 0;
      this.result.resB = 0;
      this.result.resC = 0;
      this.result.resD = 0;
      modal.classList.remove('modal--hidden');
      modal.classList.remove('modal--disable');
      end.classList.add('end--hidden');
      setTimeout(() => {
        end.classList.add('end--disable');
      }, 550);
    });
  }
}

export default End;
