class Result {
  constructor() {
    const btn = document.querySelector('.question__button');
    const checks = document.querySelectorAll('.question__input');
    const text = document.querySelector('.end__title');
    const result = document.querySelector('.end');

    this.resA = 0;
    this.resB = 0;
    this.resC = 0;
    this.resD = 0;

    btn.addEventListener('click', () => {
      checks.forEach((check) => {
        if (check.checked) {
          if (check.getAttribute('data-option') === 'A') {
            this.resA += 1;
          }
          if (check.getAttribute('data-option') === 'B') {
            this.resB += 1;
          }
          if (check.getAttribute('data-option') === 'C') {
            this.resC += 1;
          }
          if (check.getAttribute('data-option') === 'D') {
            this.resD += 1;
          }
        }
      });
      if (this.resA > 0 || this.resB > 0 || this.resC > 0 || this.resD > 0) {
        if (this.resA > this.resB && this.resA > this.resC && this.resA > this.resD) {
          document.body.classList.add('scroll-hidden');
          text.innerHTML = 'Вам подходит: WEB-ДИЗАЙНЕР';
          result.classList.remove('end--disable');
          setTimeout(() => {
            result.classList.remove('end--hidden');
          }, 550);
        }
        if (this.resB > this.resA && this.resB > this.resC && this.resB > this.resD) {
          document.body.classList.add('scroll-hidden');
          text.innerHTML = 'Вам подходит: ТЕСТИРОВЩИК ПО';
          result.classList.remove('end--disable');
          setTimeout(() => {
            result.classList.remove('end--hidden');
          }, 550);
        }
        if (this.resC > this.resA && this.resC > this.resB && this.resC > this.resD) {
          document.body.classList.add('scroll-hidden');
          text.innerHTML = 'Вам подходит: СИСТЕМНЫЙ АДМИНИСТРАТОР';
          result.classList.remove('end--disable');
          setTimeout(() => {
            result.classList.remove('end--hidden');
          }, 550);
        }
        if (this.resD > this.resA && this.resD > this.resB && this.resD > this.resC) {
          document.body.classList.add('scroll-hidden');
          text.innerHTML = 'Вам подходит: GAME DEVELOPER';
          result.classList.remove('end--disable');
          setTimeout(() => {
            result.classList.remove('end--hidden');
          }, 550);
        }

        if (this.resA === this.resB && this.resA > 0 && this.resB > 0) {
          document.body.classList.add('scroll-hidden');
          text.innerHTML = 'Вам подходят: WEB-ДИЗАЙНЕР, ТЕСТИРОВЩИК ПО';
          result.classList.remove('end--disable');
          setTimeout(() => {
            result.classList.remove('end--hidden');
          }, 550);
        }
        if (this.resA === this.resC && this.resA > 0 && this.resC > 0) {
          document.body.classList.add('scroll-hidden');
          text.innerHTML = 'Вам подходят: WEB-ДИЗАЙНЕР, СИСТЕМНЫЙ АДМИНИСТРАТОР';
          result.classList.remove('end--disable');
          setTimeout(() => {
            result.classList.remove('end--hidden');
          }, 550);
        }
        if (this.resA === this.resD && this.resA > 0 && this.resD > 0) {
          document.body.classList.add('scroll-hidden');
          text.innerHTML = 'Вам подходят: WEB-ДИЗАЙНЕР, GAME DEVELOPER';
          result.classList.remove('end--disable');
          setTimeout(() => {
            result.classList.remove('end--hidden');
          }, 550);
        }

        if (this.resB === this.resC && this.resB > 0 && this.resC > 0) {
          document.body.classList.add('scroll-hidden');
          text.innerHTML = 'Вам подходят: ТЕСТИРОВЩИК ПО, СИСТЕМНЫЙ АДМИНИСТРАТОР';
          result.classList.remove('end--disable');
          setTimeout(() => {
            result.classList.remove('end--hidden');
          }, 550);
        }
        if (this.resB === this.resD && this.resB > 0 && this.resD > 0) {
          document.body.classList.add('scroll-hidden');
          text.innerHTML = 'Вам подходят: ТЕСТИРОВЩИК ПО, GAME DEVELOPER';
          result.classList.remove('end--disable');
          setTimeout(() => {
            result.classList.remove('end--hidden');
          }, 550);
        }

        if (this.resC === this.resD && this.resC > 0 && this.resD > 0) {
          document.body.classList.add('scroll-hidden');
          text.innerHTML = 'Вам подходят: СИСТЕМНЫЙ АДМИНИСТРАТОР, GAME DEVELOPER';
          result.classList.remove('end--disable');
          setTimeout(() => {
            result.classList.remove('end--hidden');
          }, 550);
        }

        if (this.resA === this.resB
          && this.resA === this.resC
          && this.resA > 0
          && this.resB > 0
          && this.resC > 0) {
          document.body.classList.add('scroll-hidden');
          text.innerHTML = 'Вам подходят: WEB-ДИЗАЙНЕР, ТЕСТИРОВЩИК ПО, СИСТЕМНЫЙ АДМИНИСТРАТОР';
          result.classList.remove('end--disable');
          setTimeout(() => {
            result.classList.remove('end--hidden');
          }, 550);
        }
        if (this.resA === this.resB
          && this.resA === this.resD
          && this.resA > 0
          && this.resB > 0
          && this.resD > 0) {
          document.body.classList.add('scroll-hidden');
          text.innerHTML = 'Вам подходят: WEB-ДИЗАЙНЕР, ТЕСТИРОВЩИК ПО, GAME DEVELOPER';
          result.classList.remove('end--disable');
          setTimeout(() => {
            result.classList.remove('end--hidden');
          }, 550);
        }
        if (this.resA === this.resC
          && this.resA === this.resD
          && this.resA > 0
          && this.resC > 0
          && this.resD > 0) {
          document.body.classList.add('scroll-hidden');
          text.innerHTML = 'Вам подходят: WEB-ДИЗАЙНЕР, СИСТЕМНЫЙ АДМИНИСТРАТОР, GAME DEVELOPER';
          result.classList.remove('end--disable');
          setTimeout(() => {
            result.classList.remove('end--hidden');
          }, 550);
        }
        if (this.resA === this.resB && this.resB === this.resC && this.resC === this.resD) {
          document.body.classList.add('scroll-hidden');
          text.innerHTML = 'Вы разносторонняя личность! Вам подходит любая из профессий, выбор за Вами!';
          result.classList.remove('end--disable');
          setTimeout(() => {
            result.classList.remove('end--hidden');
          }, 550);
        }
      }
    });
  }
}

export default Result;
