import Result from './blocks/question/question';
import Start from './blocks/modal/modal';
import End from './blocks/end/end';

require('./autoload.scss');

new Result();
new Start();
new End();
